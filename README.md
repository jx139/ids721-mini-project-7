# Data Processing with Vector Database
![pipeline status](https://gitlab.oit.duke.edu/jx139/ids721-mini-project-7/badges/main/pipeline.svg)

## Overview

This repository contains a Rust project demonstrating a basic implementation of a vector database. The project illustrates how to ingest vector data into an in-memory database, perform simple queries and aggregations on the data, and visualize the results using a graphical chart. The primary goal of this project is to showcase a simple use case of handling and visualizing vector data with Rust.

## Features

- **Data Ingestion**: Simulate ingesting vector data into an in-memory vector database.
- **Queries and Aggregations**: Perform basic queries (e.g., finding a vector by ID) and data aggregations (e.g., calculating averages).
- **Graphical Visualization**: Visualize the vector data using a line chart to represent the vector values.

## Prerequisites

Before running this project, ensure you have the following installed:
- Rust and Cargo (the Rust package manager). You can follow the installation instructions on the [official Rust website](https://www.rust-lang.org/tools/install).
- Any additional dependencies required by the `plotters` crate for graphical rendering, depending on your operating system.

## Installation

To get started with this project, follow these steps:

1. **Clone the Repository**: Clone this repository to your local machine using Git:

   ```sh
   git clone https://example.com/vector_db_example.git
   cd vector_db_example
   ```

2. **Build the Project**: Compile the project with Cargo:

   ```sh
   cargo build
   ```

3. **Run the Project**: Execute the compiled project:

   ```sh
   cargo run
   ```

After running the project, you will see the query results and aggregation output in your console. If graphical visualization is enabled and correctly set up, a window displaying the chart of vector values will also appear.  

![build](./vector_db_example/plot.png)
![build](./vector_db_example/print.png)

## Project Structure

- `src/main.rs`: Contains the main logic for data ingestion, querying, aggregation, and visualization.
- `Cargo.toml`: Manages project dependencies and metadata.
- `README.md`: Provides an overview and guide for this repository.
