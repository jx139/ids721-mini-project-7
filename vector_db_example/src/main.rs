use plotters::prelude::*;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Debug)]
struct VectorData {
    id: u32,
    values: Vec<f64>,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let data = vec![
        VectorData {
            id: 1,
            values: vec![1.0, 2.0, 3.0],
        },
        VectorData {
            id: 2,
            values: vec![4.0, 3.0, 2.0],
        },
    ];
    let db = ingest_data(data);

    // Perform a simple query (e.g., find vector by ID)
    if let Some(vector) = db.get(&1) {
        println!("Found vector: {:?}", vector);
    }

    // Aggregate data (e.g., calculate the average of the first element in all vectors)
    let avg = calculate_average(&db);
    println!("Average of first elements: {}", avg);

    visualize(&db)?;

    Ok(())
}

fn ingest_data(data: Vec<VectorData>) -> HashMap<u32, VectorData> {
    let mut db = HashMap::new();
    for vector in data {
        db.insert(vector.id, vector);
    }
    db
}

// Calculate the average of the first element in all vectors
fn calculate_average(db: &HashMap<u32, VectorData>) -> f64 {
    let sum: f64 = db.values().map(|v| v.values[0]).sum();
    let count = db.len() as f64;
    sum / count
}

fn visualize(db: &HashMap<u32, VectorData>) -> Result<(), Box<dyn std::error::Error>> {
    let root = BitMapBackend::new("plot.png", (640, 480)).into_drawing_area();
    root.fill(&WHITE)?;
    let mut chart = ChartBuilder::on(&root)
        .caption("Vector Data Visualization", ("sans-serif", 50))
        .x_label_area_size(30)
        .y_label_area_size(30)
        .build_cartesian_2d(0..3, 0.0..6.0)?;

    chart.configure_mesh().draw()?;

    for vector in db.values() {
        chart.draw_series(LineSeries::new(
            vector
                .values
                .iter()
                .enumerate()
                .map(|(x, y)| (x as i32, *y)),
            &RED,
        ))?;
    }

    root.present()?;
    println!("Visualization saved to plot.png");
    Ok(())
}
